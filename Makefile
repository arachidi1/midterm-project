# Vasilios Nicholas 9/22/17 601.220 vnichol2 vnichol2@jhu.edu HW4
CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g 
LMFLAGs=-lm
# Links together files needed to create executable
main: main.o ppmIO.o imageManip.o menuUtil.o
	$(CC) -o main main.o ppmIO.o imageManip.o menuUtil.o -lm 

# Compiles main.c to create main.o
# Note that we list functions.h here as a file that
# main.c depends on, since main.c #includes it
main.o: main.c ppmIO.h
	$(CC) $(CFLAGS) -c main.c

# Compiles interface.c to create interface.o
# Note that we list interface.h here as a file that
# interface.c depends on, since interface.c #includes it
ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c
#
imageManip.o: imageManip.c imageManip.h
	$(CC) $(CFLAGS) -c imageManip.c

menuUtil.o: menuUtil.c menuUtil.h ppmIO.h imageManip.h
	$(CC) $(CFLAGS) -c menuUtil.c

# Removes all object files and the executable named db,
# so we can start fresh
clean:
	rm -f *.o main
