#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"
#include <math.h>
#define PI 3.14159
#define sq(x) ((x)*(x))
void grayscale(Picture* p) {
	unsigned char intensity = 0;
	for(int i = 0; i < (p->rows * p->columns); i++) {
		intensity = ((0.30 * (p->image[i].red)) + (0.59 * (p->image[i].green)) + (0.11 * (p->image[i].blue)));
		(p->image[i].red = intensity);
		(p->image[i].green = intensity);
		(p->image[i].blue = intensity);
	}
}
void swapColor(Picture *p) {
	for(int i = 0; i < (p->rows * p->columns); i++) {
		unsigned char temp1 = 0;
		temp1 = p->image[i].red;
		p->image[i].red = p->image[i].green;
		p->image[i].green = p->image[i].blue;
		p->image[i].blue = temp1;
	}
}
unsigned char checkRange(double v) {
	if(v < 0)
		return ((unsigned char) 0);
	else if (v > 255)
		return ((unsigned char) 255);
	else
		return ((unsigned char) v);
}
void brightness(Picture* p, double amount) {
	for(int i = 0; i < (p->rows * p->columns); i++) {
		p->image[i].red = checkRange(((double) p->image[i].red) + amount);
		p->image[i].green = checkRange(((double) p->image[i].green) + amount);
		p->image[i].blue = checkRange(((double) p->image[i].blue) + amount);
	}
}
double findMax(Picture* p, int index ) {
	double max = 0;
	if(index == 0) { //red
		for(int i = 0; i < (p->rows * p->columns); i++) {
			if( ((double) p->image[i].red) > max) {
				max = (double) p->image[i].red;
			}
		}
	} else if (index == 1) { //green
                for(int i = 0; i < (p->rows * p->columns); i++) {
			if( ((double) p->image[i].green) > max) {
				max = (double) p->image[i].green;
			}
		}
	} else if (index == 2) { //blue
                for(int i = 0; i < (p->rows * p->columns); i++) {
			if( ((double) p->image[i].blue) > max) {
				max = (double) p->image[i].blue;
			}
		}
	}
	return max;
}
double findMin(Picture* p, int index) {
        double min = 0;
	if(index == 0) { //red
		for(int i = 0; i < (p->rows * p->columns); i++) {
			if(((double) p->image[i].red) < min) {
				min = (double) p->image[i].red;
			}
		}
	} else if (index == 1) { //green
		for(int i = 0; i < (p->rows * p->columns); i++) {
			if(((double) p->image[i].green) < min) {
				min = (double) p->image[i].green;
			}
		}
	} else if (index == 2) { //blue
		for(int i = 0; i < (p->rows * p->columns); i++) {
			if(((double) p->image[i].blue) < min) {
				min = (double) p->image[i].blue;
			}
		}
	}
	return min;
}
void contrast(Picture* p, double factor) {
	double maxR = findMax(p, 0);
	double minR = findMin(p, 0);
	double maxG = findMax(p, 1);
	double minG = findMin(p, 1);
	double maxB = findMax(p, 2);
	double minB = findMin(p, 2);
	double rangeR = (maxR + minR)/2;
	double rangeG = (maxG + minG)/2;
	double rangeB = (maxB + minB)/2;
	for (int i = 0; i < (p->rows * p->columns); i++) {
		double red = (((((double) p->image[i].red) - rangeR)/(2 * (maxR - rangeR))) * factor);
		double green = (((((double) p->image[i].green) - rangeG)/(2 * (maxG - rangeG))) * factor);
		double blue = (((((double) p->image[i].blue) - rangeB)/(2 * (maxB - rangeB))) * factor);
		p->image[i].red = checkRange((( red * (2 * (maxR - rangeR))) + rangeR));
		p->image[i].green = checkRange(((green * (2 * (maxG - rangeG))) + rangeG));
		p->image[i].blue = checkRange(((blue * (2 * (maxB - rangeB))) + rangeB));
	}
}
void crop(Picture* p, int row1, int column1, int row2, int column2) {
	Picture* tempP = (Picture*)malloc(sizeof(Picture));
	tempP->columns = (column2 - column1);
	tempP->rows = (row2 - row1);
	if(tempP->columns <= 0 || tempP->rows <= 0 || row1 > p->rows || row2 > p->rows || column1 > p->columns || column2 > p->columns) {
		fprintf(stderr, "\nError:imageManip - invalid crop \n");
	} else {
		tempP->image = (Pixel *)malloc( sizeof(Pixel) * (tempP->columns * tempP->rows));
		tempP->color_range = 255;
		int index  = 0;
		for(int j = row1; j < row2; j++) {
			for(int i = column1; i < column2; i++) {
				tempP->image[index] = p->image[j * p->columns + i];
				index++;
			}
		}
		*p = *tempP;
		free(tempP->image);
		free(tempP);
	}
}

// Creates the filter Matrix

void gaussianGen(int size, double g[size][size], double sigma) {
	for(int y = 0; y < size; y++) {
		for(int x = 0; x < size; x++) {
			int dx = -((size)/2) +  x;
			int dy = ((size)/2) - y;
			g[y][x] = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
		}
	}
}

void filter(int size, Picture *p, double g[size][size]) {
	int r1 = 0;
	int r2 = 0;
	int c1 = 0;
	int c2 = 0;
	for(int x = 0; x < p->rows; x++) {
		for(int y = 0; y < p->columns; y++) {
			if( x < (size/2) || (p->rows - x < (size/2)) || y < (size/2) || (p->columns - y < (size/2))) {
				r1 = x - (size/2);
				r2 = x + (size/2);
				c1 = y - (size/2);
				c2 = y + (size/2);
			if(x < (size/2)) {
				r1 = 0;
			}
			if((p->rows - x) < (size/2)) {
				r2 = p->rows - 1;
			}
			if(y < (size/2)) {
				c1 = 0;
			}
			if((p->columns - y) < (size/2)) {
				c2 = p->columns - 1;
			}
			double sum[4] = {0, 0, 0, 0};
			for (int i = 0; i < size; i++) {
				for (int j = 0; j < size; j++) {
					int r = (x + i - (int)(size/2) );
					int c = (y + j - (int)(size/2));
					if(r1 <= r && r2 >= r && c1 <= c && c2 >= c) {
						sum[0] += g[i][j];
						if((r * p->columns +c) < (p->columns * p->rows)) {
						sum[1] += (((double) p->image[r * p->columns + c].red) * g[i][j]);
						sum[2] += (((double) p->image[r * p->columns + c].green) * g[i][j]);
						sum[3] += (((double) p->image[r * p->columns + c].blue) * g[i][j]);
						}
					}
					}
				}
			sum[1] /= sum[0];
			sum[2] /= sum[0];
			sum[3] /= sum[0];
				p->image[x * p->columns + y].red = checkRange(sum[1]);
				p->image[x * p->columns + y].green = checkRange(sum[2]);
				p->image[x * p->columns + y].blue = checkRange(sum[3]);
			}

			else {
				double sums[4] = {0, 0, 0, 0};
				for (int i = 0; i < size; i++) {
					for(int j = 0; j < size; j++) {
						int r = (x + i - (int)(size/2));
						int c = (y + j - (int)(size/2));
						sums[0] += g[i][j];
						if((r * p->columns +c) < (p->columns * p->rows)) {
							sums[1] += (((double) p->image[r * p->columns + c].red) * g[i][j]);
							sums[2] += (((double) p->image[r * p->columns + c].green) * g[i][j]);
                                                	sums[3] += (((double) p->image[r * p->columns + c].blue) * g[i][j]);
						}
						if ((x * p->columns + y) < (p->columns * p->rows)) {
							p->image[x * p->columns + y].red = checkRange(sums[1]/sums[0]);
							p->image[x * p->columns + y].green = checkRange(sums[2]/sums[0]);
							p->image[x * p->columns + y].blue = checkRange(sums[3]/sums[0]);
																									                                        }
					}
				}
			}
		}
	}
}

// Applies the filter to one element
void blur(double sigma, Picture *p) {
	int size = (int) (sigma * 10);
	if( size % 2 == 0) {
		size++;
	}
	double g[size][size];
	gaussianGen(size, g, sigma);
	filter(size, p, g);

}
void sharpen(double sigma, double strength, Picture *p) {
	Pixel* z = (Pixel *)malloc(sizeof(Pixel) * (p->rows * p->columns));
	for(int j = 0; j < (p->rows * p->columns); j++) {
		z[j].red = p->image[j].red;
		z[j].green = p->image[j].green;
		z[j].blue = p->image[j].blue;
	}
	blur(sigma, p);	
	for(int i = 0; i < (p->rows * p->columns); i++) {
			p->image[i].red = checkRange(((double) z[i].red) +((((double) z[i].red) - ((double) p->image[i].red)) * strength));
			p->image[i].green = checkRange(((double) z[i].green) +((((double) z[i].green) - ((double) p->image[i].green)) * strength));
			p->image[i].blue = checkRange(((double) z[i].blue) +((((double) z[i].blue) - ((double) p->image[i].blue)) * strength));
	}
	free(z);
}


void edge(double sigma, int threshold, Picture* p) {
	grayscale(p);
	blur(sigma, p);
	Pixel* z = (Pixel *)malloc(sizeof(Pixel) * (p->rows * p->columns));
	for(int j = 0; j < (p->rows * p->columns); j++) {
		z[j].red = p->image[j].red;
		z[j].green = p->image[j].green;
		z[j].blue = p->image[j].blue;
	}
	for(int x = 1; x < p->rows - 1; x++) {
		for(int y = 1; y < p->columns - 1; y++) {
			if (sqrt(sq(((double)z[(x+1) * p->columns + y].red - (double)z[(x-1) * p->columns + y].red)/2) + sq(((double)z[x * p->columns + (y + 1)].red - (double)z[x * p->columns + (y-1)].red)/2)) > threshold) {
				p->image[x * p->columns + y].red = 0;
				p->image[x * p->columns + y].green = 0;
				p->image[x * p->columns + y].blue = 0;
			} else {
				p->image[x * p->columns + y].red = 255;
				p->image[x * p->columns + y].green = 255;
				p->image[x * p->columns + y].blue = 255;
			}
		}
	}
	free(z);
}
