#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ppmIO.h"
#include "imageManip.h"
#define sq(x) ((x)*(x))
int main () {
	Picture* p = read("data/nika.ppm");
	//swapColor(p); //I don't know why, but reads in file with color values swapped twice.
	double sigma = 0.5;
	int threshold = 8;
	grayscale(p);
        blur(sigma, p);
        Pixel* z = (Pixel *)malloc(sizeof(Pixel) * (p->rows * p->columns));
        for(int j = 0; j < (p->rows * p->columns); j++) {
                z[j].red = p->image[j].red;
                z[j].green = p->image[j].green;
                z[j].blue = p->image[j].blue;
        }
        for(int x = 1; x < p->rows - 1; x++) {
                for(int y = 1; y < p->columns - 1; y++) {
                        if ( (int)sqrt(sq((z[((x+1) * p->columns) + y].red - z[((x-1) * p->columns) + y].red)/2) + sq((z[(x * p->columns) + (y + 1)].red - z[(x * p->columns) + (y-1)].red)/2)) > threshold) {
                                p->image[x * p->columns + y].red = 0;grayscale(p);
        blur(sigma, p);
        Pixel* z = (Pixel *)malloc(sizeof(Pixel) * (p->rows * p->columns));
        for(int j = 0; j < (p->rows * p->columns); j++) {
                z[j].red = p->image[j].red;
                z[j].green = p->image[j].green;
                z[j].blue = p->image[j].blue;
        }
        printf("\n%d %d\n", p->rows, p->columns);
        for(int x = 1; x < p->rows - 1; x++) {
                for(int y = 1; y < p->columns - 1; y++) {
                        printf("\n%d\n",sqrt(sq((z[(x+1) * p->columns + y].red - z[(x-1) * p->columns + y].red)/2) + sq((z[x * p->columns + (y + 1)].red - z[x * p->columns + (y-1)].red)/2)));
                        if (sqrt(sq((z[(x+1) * p->columns + y].red - z[(x-1) * p->columns + y].red)/2) + sq((z[x * p->columns + (y + 1)].red - z[x * p->columns + (y-1)].red)/2)) > threshold) {
                                p->image[x * p->columns + y].red = 0;
                                p->image[x * p->columns + y].green = 0;
                                p->image[x * p->columns + y].blue = 0;
                        } else {
                                p->image[x * p->columns + y].red = 255;
                                p->image[x * p->columns + y].green = 255;
                                p->image[x * p->columns + y].blue = 255;
                        }
                }
        }
                                p->image[x * p->columns + y].green = 0;
                                p->image[x * p->columns + y].blue = 0;
                        } else {
                                p->image[x * p->columns + y].red = 255;
                                p->image[x * p->columns + y].green = 255;
                                p->image[x * p->columns + y].blue = 255;
                        }
                }
        }


	//blur(p, 0.5);
	write(p, "data/nika1.ppm");
	
	return 0;
}
