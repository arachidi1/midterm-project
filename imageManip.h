#ifndef imageManip
#define imageManip
void swapColor(Picture* p);
void brightness(Picture* p, double amount);
unsigned char checkRange(double v);
void grayscale(Picture* p);
double findMax(Picture* p, int index);
double findMin(Picture* p, int index);
void contrast(Picture* p, double factor);
void crop(Picture* p, int row1, int column1, int row2, int column2);
void gaussianGen(int size, double g[size][size], double sigma);
void filter(int size, Picture *p, double g[size][size]);
void blur(double sigma, Picture *p);
void sharpen(double sigma, double strength, Picture *p);
void edge(double sigma, int threshold, Picture* p);
#endif
