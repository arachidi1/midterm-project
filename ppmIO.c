#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"
Picture* read(char* file_name) {
	char id[] = "";
	Picture* p = NULL;
	FILE* f1 = fopen(file_name, "rb");
	if(!f1) {
		fprintf(stderr, "\nError:ppmIO - read given a bad filehandle %s\n", file_name);
	}
	else {
		fscanf(f1, "%s", id);
		if(strcmp(id, "P6") != 0)  {
			fprintf(stderr, "\nError:ppmIO - file format not P6 \n");
		}
		else {
			Picture* p = (Picture *)malloc(sizeof(Picture));
			char c = fgetc(f1);
			int count = 0;
       			while(c == '#') {
				while(fgetc(f1) != '\n') {
					count++;
				}
				c = fgetc(f1);
			}
			ungetc(c, f1);
			//int count = 0;
			char j = fgetc(f1);
			while(fgetc(f1) != '\n') {
				count++;
			}
			ungetc(j, f1);
			if(fscanf(f1, "%d", &p->columns) != 0){
				
				fscanf(f1, "%d", &p->rows);
				fscanf(f1, "%d", &p->color_range);
			} else {
				fscanf(f1, "%d", &p->columns);
				fscanf(f1, "%d", &p->rows);
				fscanf(f1, "%d", &p->color_range);
			}
			if(p->columns <= 0|| p->rows <= 0 || p->color_range != 255) {
				fprintf(stderr, "\nError:ppmIO - Error reading file - dimensions are 0 or color_range not 255.\n");
				return NULL;
			}
			//char j = fgetc(f1);
			while( fgetc(f1) != '\n') {
                        	count++;
			}
			p->image = (Pixel *)malloc( sizeof(Pixel) * (p->columns * p->rows));
			fread(p->image, sizeof(Pixel), p->columns * p->rows, f1);
			fclose(f1);
			return p;
		}
	}
	return p;
}
int write(Picture* p, char* file_name) {
	FILE* f1 = fopen(file_name, "wb");
	if (!f1) {
		fprintf(stderr, "\nError:ppmIO - write given a bad filehandle\n");
		return 0;
	}
	
	/* write tag */
	fprintf(f1, "P6\n #Made by Vasilios and Ali\n");

	/* write dimensions */
	fprintf(f1, "%d %d %d\n", p->columns, p->rows, p->color_range);

	/* write pixels */
	int written = fwrite(p->image, sizeof(Pixel), p->columns * p->rows, f1);
	if (written != p->columns * p->rows) {
		fprintf(stderr, "\nError:ppmIO - failed to write data to file!\n");
	}
	fclose(f1);
	return written;
}
