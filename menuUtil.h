// Ali Rachidi    601.220    9/26/2017    arachid1   arachid1@jhu.edu   HW4

#ifndef INTERFACE_H
#define INTERFACE_H

/**
 * User interface with functions
 * for exact output and prompt formatting
 */

#include <stdio.h>
#include <stdlib.h>

/**
 * Print all menu options to the user
 */
void print_menu();

/**
 * Bad prompt
 */

void reading();
void bad_prompt(char* prompt);

/**
 * bad amount
 */

void bad_amount();

/**
 * bad corners
 */

void bad_corners(int UpperRow, int UpperColumn, int LowerRow, int LowerColumn);

/**
 * bad sigma
 */

void bad_sigma(int sigma);


/**
 * bad threshold
 */

void bad_threshold(int threshold);
void not_enough_corners();

void image_not_loaded();
#endif
// end of header guard
