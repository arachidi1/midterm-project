#ifndef ppmIO
#define ppmIO
#include <stdio.h>
#include <stdlib.h>
typedef struct _pixel{
	unsigned char red;
	unsigned char green;
	unsigned char blue;
} Pixel;
typedef struct _picture{
	int columns;
	int rows;
	int color_range;
	Pixel* image;
} Picture;
Picture* read(char* file_name);
int write(Picture* p, char* file_name);
#endif
