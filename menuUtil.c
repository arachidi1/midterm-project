// Ali Rachidi    601.220    9/26/2017    arachid1   arachid1@jhu.edu   HW4

/**
* Implementation of the user interface functions
* as described in the interface header file
*/

#include "menuUtil.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "ppmIO.h"
#include "imageManip.h"

void reading() {
    char str1[] = "";
    char str2[] = "";
    char input[] = "";
    double amount = 0;
    char* token = "";
    int array[5] = {-1, -1, -1, -1, -1};
    double array1[3] = {-1, -1, -1};
	char delim[] = " ";
	char* str3 = "";
	int i = 0;
	int changed = 1;
    print_menu();
    Picture* p = NULL;
    while (scanf("%s", str1)) {
        if(strcmp(str1,"q") == 0) {
		if (p != NULL) { 
			free(p->image);
			free(p);
		}
		break;
	}
	else if (strcmp(str1,"r") == 0) {
		scanf(" %s", str2);
        	p = read(str2);
	    /*if (scanf("%s", str2) = 0 || fopen("str2", "r") == 0) {
                bad_filename();
            }
            else {
              	 
            }*/
        }
	else if (strcmp(str1,"w") == 0 && p != NULL) {
		scanf("%s", str2);
		int x = write(p, str2);
		if ( x != -1) {
			printf("\n Image written to file \n");	
		}
        }
	else if (strcmp(str1, "s") == 0 && p != NULL) {
		swapColor(p);
	}
	else if (strcmp(str1, "br") == 0 && p != NULL) {
          	fgets(str2, 255, stdin); 
		strcpy(input, strtok(str2, " "));	
		amount = atof(input);
		if(amount != 0) {
			brightness(p, amount);
		}
	  	else { 
                	bad_amount();
           	}
        }
	else if (strcmp(str1, "c") == 0  && p != NULL) {

	    	fgets(str2, 255, stdin);
		for (token = strtok(str2, delim); token; token = strtok(NULL, delim))
		{
			if (i < 5) {
				array[i] = (int) strtol(token, &str3, 10);
				if(str3 == token) {
					fprintf(stderr, "\nError:menuUtil - string entered please only enter numbers.\n");
					changed = 0;
					break;
				}
				i++;
			}
		}  
            	for (int i = 0; i < 5; i++) {
                	if (array[i] < 0 && i < 4) {
                    		not_enough_corners();
		    		changed = 0;
                   		break;
                	} else if (i == 5 && array[i] != -1) {
				fprintf(stderr, "\nError:menuUtil - Too many corners entered\n");	
				changed = 0;
				break;
			}
            	}
	    	if(changed) {
	    		crop(p, array[0], array[1], array[2], array[3]);
	    	}
        }

	else if (strcmp(str1, "g") == 0 && p != NULL) {
        	grayscale(p);
	}
	else if (strcmp(str1, "cn") == 0 && p != NULL) {
                fgets(str2, 255, stdin);
	        strcpy(input, strtok(str2, " "));
		amount = atof(input);
		if(amount != 0) {
			contrast(p, amount);
		}
		else {
			bad_amount();
		}	
		// ADD FUNCTION FOR contrast
                        // should we account for different data type? if yes:
                    //    if (scanf("d", amount) == 0) {
                    //        bad_amount(amount);
                    // }
        }
	else if (strcmp(str1, "bl") == 0 && p != NULL) {
            	fgets(str2, 255, stdin);
	    	strcpy(input, strtok(str2, " "));
		amount = atof(input);
		if(amount != 0) {
			blur(amount, p);
		}
		else {
			bad_amount();
		}
        }
	else if (strcmp(str1, "sh") == 0 && p != NULL) {
		fgets(str2, 255, stdin);
		i = 0;
		for (token = strtok(str2, delim); token; token = strtok(NULL, delim)) {
			if(i < 3) {
				array1[i] = strtod(token, &str3);
				if(str3 == token) {
					fprintf(stderr, "\nError:menuUtil - string entered please only enter numbers.\n");
					changed = 0;
					break;
				}	
			}
			i++;
		}
		for (int j = 0; j < 3; j++) {
			if(array1[j] < 0 && j < 2) {
				fprintf(stderr, "\nError:menuUtil - not enough numbers entered\n");
				changed = 0;
				break;
			} else if ( j == 3 && array1[j] != -1) {
				fprintf(stderr, "\nError:menuUtil - Too many numbers entered please enter only 2 numbers\n");
				changed = 0;
				break;
			}
		}
		if(changed) {
		         sharpen(array1[0], array1[1], p);
		}

	}
	else if (strcmp(str1, "e") == 0 && p != NULL) {
		fgets(str2, 255, stdin);
		i = 0;
		for (token = strtok(str2, delim); token; token = strtok(NULL, delim)) {
			if(i < 3) {
				array1[i] = strtod(token, &str3);
				if(str3 == token) {
					fprintf(stderr, "\nError:menuUtil - string entered please only enter numbers.\n");
					changed = 0;
					break;
				}
			}
			i++;
		}
		for (int j = 0; j < 3; j++) {
			if(array1[j] < 0 && j < 2) {
				fprintf(stderr, "\nError:menuUtil - not enough numbers entered\n");
				changed = 0;
				break;
			} else if ( j == 3 && array1[j] != -1) {
				fprintf(stderr, "\nError:menuUtil - Too many numbers entered please enter only 2 numbers\n");
				changed = 0;
				break;
			}
		}
		if(array1[1] * 10 - (((int) array1[1]) * 10) != 0) {
			fprintf(stderr, "\nError:menuUtil - Please enter an integer value for the threshold, not a decimal\n");
		        changed = 0;
		}
		if(changed) {
			edge(array1[0], ((int) array1[1]), p);
		}	 
	}
	else if (p == NULL) {
		image_not_loaded();
	}
	else{
		bad_prompt(str1);
	}
    	print_menu();
        }
    }

void print_menu() {
    printf(
        "r <filename> - read image from <filename>\n"
        "w <filename> - write image to <filename>\n"
        "s - swap color channels\n"
        "br <amt> - change brightness (up or down) by the given amount\n"
        "c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n"
        "g - convert to grayscale \n"
        "cn <amt> - change contrast (up or down) by the given amount\n"
        "bl <sigma> - Gaussian blur with the given radius (sigma)\n"
        "sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n"
        "e <sigma> <threshold> - detect edges with intensity gradient above given threshold\n"
        "q - quit\n"
        "Enter choice: ");
}

void bad_prompt(char* prompt) {
    printf("\nBad prompt: %s \n", prompt);
}

void bad_amount() {
    printf("\nThe amount must be an integer. Please, try again!\n");
}

void bad_corners(int UpperRow, int UpperColumn, int LowerRow, int LowerColumn) {
    printf("\nBad corners: (%d, %d) , (%d, %d)\n", UpperRow, UpperColumn, LowerRow, LowerColumn);
}

void not_enough_corners() {
    printf("\nYou didn't enter enough values or one of your values is negative. Please, try again!\n");
}


void bad_sigma(int sigma) {
    printf("\nBad sigma: %d\n", sigma);
}

void bad_threshold(int threshold) {
    printf("\nBad threshold: %d\n", threshold);
}
void image_not_loaded() {
	fprintf(stderr, "\nError:menuUtil - No image loaded to manipulate. Please load an image with r <filename>\n");
}
